--[[
  WAOW - An impromptu game by Jean-Claude Paquin

  Wait, why are you reading this?
--]]

Lovebird = require("lib/lovebird")

Vector = require('lib/hump/vector')

-- Class system

Class = require("lib/hump/class")

-- Rendering system

Color = require("render/color")
ClipStack = require("render/clipstack")
Renderable = require("render/renderable")

-- State system

Gamestate = require("lib/hump/gamestate")
States = {}

function LoadState(stateName)
  if not States[stateName] then
    States[stateName] = require("states/" .. stateName)
  end

  return States[stateName]
end

-- Love system callbacks

function love.load()
  Gamestate.registerEvents()
  Gamestate.switch(LoadState("menu"))
end

function love.update()
  Lovebird.update()
end
