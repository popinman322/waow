--[[
  Renderable object base class
--]]

local renderable = Class {
  init = function(self, x, y, width, height, children)
    self.x = x or 0
    self.y = y or 0
    self.width = width or 0
    self.height = height or 0
    self.backgroundColor = Color.Black
    self.foregroundColor = Color.White

    self.children = children or {}
  end,

  render = function(self)
    for _, child in ipairs(self.children) do
      child:render()
    end
  end,
}
