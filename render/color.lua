--[[
  Color object
--]]

local function optionalSet(index)
  return function(self, val)
    if val then
      self[index] = val
    end
    return tbl[index]
  end
end

local color = Class {
  init = function(self, red, green, blue, alpha)
    self[1] = red or 0
    self[2] = green or 0
    self[3] = blue or 0
    self[4] = alpha or 255
  end,

  red = optionalSet(1),
  green = optionalSet(2),
  blue = optionalSet(3),
  alpha = optionalSet(4)
}

color.White = color(255, 255, 255)
color.Black = color(  0,   0,   0)
color.Red   = color(255,   0,   0)
color.Green = color(  0, 255,   0)
color.Blue  = color(  0,   0, 255)

return color
