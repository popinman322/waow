--[[
  Clip stack implementation to control rendering regions
--]]

local clipstack = Class {
  init = function(self)
    self.stack = {}
  end,

  -- Clips with translation based on the current clip stack
  clipTo = function(self, x, y, width, height)

  end,

  pop = function(self)

  end,
}
